﻿namespace RapidGetLatest.DAL
{
    using Ninject.Modules;

    /// <summary>
    /// Defines bindings for Data Access layer
    /// </summary>
    public class NinjectDataAccessModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IFileManager>().To<FileManager>().InSingletonScope();
        }
    }
}
