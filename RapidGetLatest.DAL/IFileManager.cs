﻿namespace RapidGetLatest.DAL
{
    public interface IFileManager
    {
        string TfsUrl { get; }

        string ProjectPath { get; }
    }
}
