﻿namespace RapidGetLatest.DAL
{
    using System.IO;

    /// <summary>
    /// Reads project url and path from file
    /// </summary>
    internal class FileManager : IFileManager
    {
        public string TfsUrl { get; }

        public string ProjectPath { get; }

        public FileManager()
        {
            const int TFS_URL_LINE = 0;
            const int PROJECT_PATH_LINE = 1;

            var lines = File.ReadAllLines($"{Directory.GetCurrentDirectory()}/setup.ini");

            TfsUrl = lines[TFS_URL_LINE];
            ProjectPath = lines[PROJECT_PATH_LINE];
        }
    }
}
