﻿namespace RapidGetLatest.Services
{
    using RapidGetLatest.Domain;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IRapidGetLatestService
    {
        List<SourceFile> GetOutdatedFiles();

        Task<List<SourceFile>> GetOutdatedFilesAsync();
    }
}
