﻿namespace RapidGetLatest.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.TeamFoundation.Client;
    using Microsoft.TeamFoundation.VersionControl.Client;
    using RapidGetLatest.Domain;
    using RapidGetLatest.DAL;

    /// <summary>
    /// Defines an implementation of <see cref="IRapidGetLatestService"/> contract.
    /// </summary>
    internal class TfsService : IRapidGetLatestService
    {
        private readonly IFileManager _fileManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TfsService"/> class.
        /// </summary>
        /// <param name="fileManager">Instance of class which implements <see cref="IFileManager"/>.</param>
        public TfsService(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }

        /// <summary>
        /// Asynchronously get the list of files which need to be updated
        /// </summary>
        /// <returns>Task to await</returns>
        public Task<List<SourceFile>> GetOutdatedFilesAsync()
        {
            return Task.Factory.StartNew(GetOutdatedFiles);
        }

        /// <summary>
        /// Gets the list of files which need to be updated
        /// </summary>
        /// <returns>The list of not latest files</returns>
        public List<SourceFile> GetOutdatedFiles()
        {
            string tfsUrl = _fileManager.TfsUrl;
            string projectPath = _fileManager.ProjectPath;

            var server = RegisteredTfsConnections.GetProjectCollection(new Uri(tfsUrl));
            var projects = TfsTeamProjectCollectionFactory.GetTeamProjectCollection(server);
            var sourceControl = (VersionControlServer)projects.GetService(typeof(VersionControlServer));

            var workspace = sourceControl.QueryWorkspaces(null, sourceControl.AuthorizedUser, Environment.MachineName).First();
            var folder = workspace.Folders.First(f => projectPath.Contains(f.ServerItem) && !f.IsCloaked);

            var localVersions = workspace.GetLocalVersions(new[] { new ItemSpec(folder.LocalItem, RecursionType.Full) }, false)
                                         .Single()
                                         .ToDictionary(item => item.Item, item => item.Version);

            return sourceControl.GetItems(projectPath + "/*", RecursionType.Full)
                                .Items
                                .Where((item) =>
                                {
                                    var localItem = workspace.GetLocalItemForServerItem(item.ServerItem);
                                    return item.ItemType == ItemType.File
                                        && localVersions.ContainsKey(localItem)
                                        && item.ChangesetId != localVersions[localItem];
                                })
                                .Select(GetSourceFile)
                                .ToList();
        }

        /// <summary>
        /// Gets the corresponding <see cref="SourceFile"/> for the specified <see cref="Item"/>
        /// </summary>
        /// <param name="item">Item which represents the file on server</param>
        /// <returns>SourceFile which represents local file</returns>
        private SourceFile GetSourceFile(Item item)
        {
            const int TARGET_PART = 5;
            const int LIBRARY_PART = 6;
            const int FILE_NAME_PART = 7;

            var parts = item.ServerItem.Split('/').ToArray();
            return new SourceFile
            {
                Target = parts[TARGET_PART],
                Library = parts[LIBRARY_PART],
                Name = parts.Length > FILE_NAME_PART ? parts[FILE_NAME_PART] : null
            };
        }
    }
}
