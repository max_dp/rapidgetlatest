﻿namespace RapidGetLatest.Services
{
    using Ninject.Modules;

    /// <summary>
    /// Defines bindings for Service layer
    /// </summary>
    public class NinjectServiceBindModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IRapidGetLatestService>().To<TfsService>().InSingletonScope();
        }
    }
}
