﻿namespace RapidGetLatest.UI
{
    using System.Windows;
    using RapidGetLatest.Services;
    using RapidGetLatest.DAL;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            IocKernel.Initialize(new NinjectDataAccessModule(), 
                                 new NinjectServiceBindModule());

            base.OnStartup(e);
        }
    }
}
