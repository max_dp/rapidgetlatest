﻿namespace RapidGetLatest.UI
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using RapidGetLatest.Domain;
    using RapidGetLatest.Services;
    using AppResources = Properties.Resources;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Refresh(object sender, RoutedEventArgs e)
        {
            codeSetsTree.Items.Clear();
            resultsCanvas.Visibility = Visibility.Hidden;
            loadingCanvas.Visibility = Visibility.Visible;

            try
            {
                var rapidGetLatestService = IocKernel.Get<IRapidGetLatestService>();
                var files = await rapidGetLatestService.GetOutdatedFilesAsync();

                UpdateTree(files);
                resultsCanvas.Visibility = Visibility.Visible;
            }
            catch (FileNotFoundException ex)
            {
                errorCanvas.Visibility = Visibility.Visible;
                errorText.Text = string.Format(AppResources.SettingFileNotFound, ex.FileName);
            }
            catch (Exception ex)
            {
                errorCanvas.Visibility = Visibility.Visible;
                errorText.Text = string.Format(AppResources.ErrorMessage, ex.GetType().Name, ex.Message, ex.StackTrace);
            }
            finally
            {
                loadingCanvas.Visibility = Visibility.Hidden;
            }            
        }

        private void UpdateTree(List<SourceFile> files)
        {
            if (files.Count == 0)
            {
                infoLabel.Content = AppResources.AllUpToDate;
            }
            else
            {
                infoLabel.Content = AppResources.TreeDescription;
                files.GroupBy(f => f.Target)
                     .ToList()
                     .ForEach(target =>
                     {
                         var targetTreeItem = new TreeViewItem { Header = target.Key };

                         target.GroupBy(t => t.Library)
                               .ToList()
                               .ForEach(library =>
                               {
                                   var libraryTreeItem = new TreeViewItem { Header = library.Key };

                                   library.Where(file => file.Name != null)
                                          .ToList()
                                          .ForEach(file => libraryTreeItem.Items.Add(new TreeViewItem { Header = file.Name }));
                                   targetTreeItem.Items.Add(libraryTreeItem);
                               });
                         codeSetsTree.Items.Add(targetTreeItem);
                     });
            }
        }
    }
}
