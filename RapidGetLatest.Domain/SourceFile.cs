﻿namespace RapidGetLatest.Domain
{
    /// <summary>
    /// Represents trackable locally stored file
    /// </summary>
    public class SourceFile
    {
        public string Target { get; set; }

        public string Library { get; set; }

        public string Name { get; set; }
    }
}
